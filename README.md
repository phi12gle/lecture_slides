## Repository for Lecture Slides of Introduction to Digital Humanities

### WS 2016/2017

* [1. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/61e17abd40b1ddc03a8aa5abc88eb23cf08fc0e3/IntroDH_20161010.pdf)
* [2. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161017.pdf)
* [3. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161024.pdf)
* [4. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161107.pdf)
* [5. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161114.pdf)
* [6. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161121.pdf)
* [7. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161128.pdf)
* [8. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161205.pdf)
* [9. Lecture] (https://git.informatik.uni-leipzig.de/introduction-dh/lecture_slides/blob/master/IntroDH_20161212.pdf)


### Additional Material

* Matt Munson's slides from his eHumanities Seminar [Talk] (https://docs.google.com/presentation/d/13fbGhHYxbewrnw_2BSIh2jlMTu04tzJ9e5jOqu3SxUc/edit?usp=sharing)